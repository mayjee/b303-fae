const Course = require("../models/Course");
const User = require("../models/User");




//[SECTION] Create a new course
/*
	Steps:
	1. Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	2. Uses the information from the request body to provide all the necessary information.
	3. Saves the created object to our database and add a successful validation true/false.
*/
module.exports.addCourse = (req, res) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	});

	// Saves the created object to our database
	return newCourse.save().then((course, error) => {

		// Course creation successful
		if (error) {
			return res.send(false);

		// Course creation failed
		} else {
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};


// Retrieve all courses
/*
	Business Logic:
		1. Retrieve all the courses from the courses collection from the database.

*/


// We used the find() method of our Course model.
// Because our Course model is connected to our courses collection
// Course.find({}) is the same as db.courses.find({})
// empty {} will return all the documents from the courses collection.
module.exports.getAllCourses = (req, res) => {
	return Course.find({}).then(result => {
		// console.log(result)
		return res.send(result);
	})
	.catch(err => res.send(err))
};


// Retrieve All ACTIVE Courses
/*
	Business Logic:
		1. Retrieve all the courses from the database with property of "isActive" equals to true.
*/

module.exports.getAllActive = (req, res) => {
	return Course.find({ isActive: true }).then(result => {
		// console.log(result)
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Retrieve a Specific Course
/*
	Business Logic:
		1. Retrieve a specific course from the courses collection based on the document's object ID that we received from the url params.
*/

module.exports.getCourse = (req, res) => {
	return Course.findById(req.params.courseId).then(result => {
		console.log(result)
		return res.send(result)
	})
	.catch(err => res.send(err))
};

// Update Course:

/*
	Business Logic:
		1. Create an "updatedCourse" variable and use the properties from the mongoose model to pass the new values.
		2. Save the updatedCourse back into the courses collection. Find the object first by its id before saving the changes.
		3. Return a true response if saving is successful and return false if not.
*/
module.exports.updateCourse = (req, res) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	// console.log(updatedCourse);
	// Syntax:
		// findByIdAndUpdate(documentId, updatesToBeApplied)
	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) => {


		// Course is not updated
		if(error) {
			return res.send(false);
		// Course updated successfully
		} else {
			// console.log(course)
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};


//[SECTION] Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveCourse = (req, res) => {

	let updateActiveField = {
		isActive: false
	}

	return Course.findByIdAndUpdate(req.params.courseId, updateActiveField)
	.then((course, error) => {

		//course archived successfully
		if(error){
			return res.send(false)

		// failed
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))

};



//[SECTION] Activate a course
module.exports.activateCourse = (req, res) => {

	let updateActiveField = {
		isActive: true
	}

	return Course.findByIdAndUpdate(req.params.courseId, updateActiveField)
	.then((course, error) => {

		//course archived successfully
		if(error){
			return res.send(false)

		// failed
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))

};



// Controller action to search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Retrieve emails of enrolled users in a course.
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const courseId = req.params.courseId; // Use req.params instead of req.body

  try {
    // Find the course by courseId
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: 'Course not found' });
    }

    // Get the userIds of enrolled users from the course
    const userIds = course.enrollees.map(enrollee => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Use userIds variable instead of undefined "users"

    // Extract the emails from the enrolled users
    const emails = enrolledUsers.map(user => user.email); // Use map instead of forEach

    res.status(200).json({ userEmails: emails }); // Use the correct variable name userEmails instead of emails
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};



//[ACTIVITY]

module.exports.searchCoursesByPriceRange = async (req, res) => {
     try {
       const { minPrice, maxPrice } = req.body;
  
          // Find courses within the price range
       const courses = await Course.find({
        price: { $gte: minPrice, $lte: maxPrice }
      });
      
      res.status(200).json({ courses });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching for courses' });
    }
   };