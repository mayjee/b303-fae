// import coursesData from '../data/coursesData.js';
import { useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView.js';
// import CourseCard from '../components/CourseCard.js';
import UserView from '../components/UserView.js';
import UserContext from '../UserContext';

export default function Courses() {
	// Get user details from UserContext
	const { user } = useContext(UserContext);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// Retrieves the courses from the DB upon initial render of the "Courses" component
	// useEffect(() => {
	// 	fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);
        

	// 		// setCourses(data.map(course_item => {
	// 		// 	return (
	// 		// 		<CourseCard key={course_item._id} course={course_item}/>
	// 		// 	)
	// 		// }))



    //     	// Set the fetched course data directly in the state
	// 		setCourses(data);
	// 	});
	// }, []);


	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
        
			setCourses(data);
		})
	};


	useEffect(() => {

		fetchData();
	}, []);




	return (
		<>

			{user.isAdmin === true ?
		
				<AdminView coursesData={courses} fetchData={fetchData} />
			:
			
				<UserView coursesData={courses} />
			}

			{/* hard coded */}
			{/* <CourseCard course={coursesData[0]}/>
			<CourseCard course={coursesData[1]}/> */}
		</>
	);
}

