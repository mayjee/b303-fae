// import { Button } from 'react-bootstrap';
// my answer

// import { Navigate } from 'react-router-dom';
// import Banner from '../components/Banner.js';


// export default function Error() {


// 	const navigateToHome = () => {

// 		return (
// 		// redirect the user back to the home page
// 			<Navigate to="/" />
// 		)
// 	}


// 	return (
// 		<Banner
// 			title="404 - Page Not Found"
// 			subtitle="The page you are looking for cannot be found"
// 			buttonText="Back home"
// 			buttonOnClick={navigateToHome}
// 		/>
// 	);
// }



// solution
import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}
