import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {

	// localStorage.clear();

	const { unsetUser, setUser } = useContext(UserContext);


	// to clear the localStorage upon logout
	// function is in the App.js, line 26
	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		});
	}, [setUser])


	return (
		// redirect the user back to the login page
		<Navigate to="/login" />
	)
}