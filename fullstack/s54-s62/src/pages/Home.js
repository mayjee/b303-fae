// my answer
// import { Navigate } from 'react-router-dom';
// import Banner from '../components/Banner.js';
// import Highlights from '../components/Highlights.js';

// export default function Home() {

// 	const navigateToCourses = () => {

// 		return (
// 			<Navigate to="/courses" />
// 		)
// 	}


// 	return (
// 		<>
// 			<Banner
// 				title="Suguru's Monkey Academy"
// 				subtitle="Monkey for everyone!"
// 				buttonText="Enroll Now!"
// 				buttonOnClick={navigateToCourses}
// 			/>
// 			<Highlights/>
// 		</>
// 	)
// }


// solution
import Banner from '../components/Banner.js';
import FeaturedCourses from '../components/FeaturedCourses.js';
import Highlights from '../components/Highlights.js';

export default function Home(){

	const data = {
	    title: "Zuitt Coding Bootcamp",
	    content: "Opportunities for everyone, everywhere",
	    destination: "/courses",
	    label: "Enroll now!"
	}

	return(
		<>
			<Banner data={data}/>
			<FeaturedCourses />
			<Highlights/>
		</>
	)
}
