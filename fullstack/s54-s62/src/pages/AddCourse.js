// import { useState, useEffect, useContext } from 'react';
// import { Form, Button } from 'react-bootstrap';
// import { Navigate, useNavigate } from 'react-router-dom';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';


// export default function AddCourse() {

// 	const { user } = useContext(UserContext);

// 	const navigate = useNavigate();

// 	const isAdmin = user.isAdmin === true;

// 	const [name, setName] = useState("");
// 	const [description, setDescription] = useState("");
// 	const [price, setPrice] = useState("");
// 	// isActive is for the button -- called conditional rendering
// 	const [isActive, setIsActive] = useState(false);


// 	function addCourse(event) {

// 		event.preventDefault();

// 		fetch(`${process.env.REACT_APP_API_URL}/addCourse`, {
// 			method: 'POST',
// 			headers: {
// 				'Content-Type': 'application/json',
// 				Authorization: `Bearer ${localStorage.getItem('token')}`
// 			},
// 			body: JSON.stringify({
				
// 				name: name,
// 				description: description,
// 				price: price
// 			})
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			// console.log(data)
// 			console.log(data)

			
// 			// if (data === true)

// 			if (data === true) {

// 				setName("")
// 				setDescription("")
// 				setPrice("")

// 				Swal.fire({
// 					title: "Course Added",
// 					icon: "success",
// 					text: "Your course has been added succesfully"
// 				})

// 				// Redirect the user back to Courses page after successful addition of course
// 				navigate("/courses");

// 			} else {
// 				Swal.fire({
// 					title: "Unsuccessful Course Creation",
// 					icon: "error",
// 					text: "Please try again"
// 				})
// 			}
// 		})

// 	}


	
// 	useEffect(() => {
// 		if (name !== "" && description !== "" && price !== "") {

// 			setIsActive(true)

// 		} else {
// 			setIsActive(false)
// 		}

// 	}, [name, description, price]);


// 	return (

// 		(!isAdmin) ?
// 			<Navigate to={`/courses`} />

// 		:

// 			<Form onSubmit={(event) => addCourse(event)}>

// 	            <h1 className="my-5 text-center">Add Course</h1>
// 	                <Form.Group>
// 	                    <Form.Label>Name:</Form.Label>
// 	                    <Form.Control
// 	                    	type="text"
// 	                    	placeholder="Enter Name"
// 	                    	required
// 	                    	// two-way data binding
// 	                    	value={name}
// 	                    	onChange={event => {setName(event.target.value)}}
// 	                    />
// 	                </Form.Group>
// 	                <Form.Group>
// 	                    <Form.Label>Description:</Form.Label>
// 	                    <Form.Control
// 	                    	type="text"
// 	                    	placeholder="Enter Description"
// 	                    	required
// 	                    	value={description}
// 	                    	onChange={event => {setDescription(event.target.value)}}
// 	                    />
// 	                </Form.Group>
// 	                <Form.Group>
// 	                    <Form.Label>Price:</Form.Label>
// 	                    <Form.Control
// 	                    	type="number"
// 	                    	placeholder="Enter Price"
// 	                    	required
// 	                    	value={price}
// 	                    	onChange={event => {setPrice(event.target.value)}}
// 	                    />
// 	                </Form.Group>
	           
// 	                <Button variant="primary" type="submit" disabled={isActive === false}>Submit</Button>
			                        
// 			</Form>
// 	)



// }




// solution
import {useState,useEffect, useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddCourse(){

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    //input states
    const [name,setName] = useState("");
    const [description,setDescription] = useState("");
    const [price,setPrice] = useState("");
    const [isActive,setIsActive] = useState(false);

    function createCourse(e){

        //prevent submit event's default behavior
        e.preventDefault();

        let token = localStorage.getItem('token');
        console.log(token);

        fetch('http://localhost:4000/courses/',{

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({

                name: name,
                description: description,
                price: price

            })
        })
        .then(res => res.json())
        .then(data => {

            //data is the response of the api/server after it's been process as JS object through our res.json() method.
            console.log(data);

            if(data){
                Swal.fire({

                    icon:"success",
                    title: "Course Added"

                })

                navigate("/courses");
            } else {
                Swal.fire({

                    icon: "error",
                    title: "Unsuccessful Course Creation",
                    text: data.message

                })
            }

        })

        setName("")
        setDescription("")
        setPrice(0);
    };

    useEffect(() => {
        if(name !== "" && description !== "" && price !== "") {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price])



    return (

            (user.id === null || user.isAdmin === false)
            ?
            <Navigate to="/courses" />
            :
            <>
                <h1 className="my-5 text-center">Add Course</h1>
                <Form onSubmit={e => createCourse(e)}>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name" required value={name} onChange={e => {setName(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
                    </Form.Group>
                    {
                        isActive === true ?
                            <Button variant="primary" type="submit" className="my-5">Submit</Button>
                            :
                            <Button variant="danger" type="submit" className="my-5" disabled>Submit</Button>
                    }
                </Form>
            </>

    )


}
