import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar.js';
import AddCourse from './pages/AddCourse.js';
import Courses from './pages/Courses.js';
import CourseView from './pages/CourseView.js';
import Error from './pages/Error.js';
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Profile from './pages/Profile.js';
import Register from './pages/Register.js';
import './App.css';
import { UserProvider } from './UserContext';



// the 'App.js' component is where we usually import other custom components
// when putting two or more components together, we have to use a container for it to work properly
function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // Function for clearing local storage upon logout
  const unsetUser = () => {
    localStorage.clear();
  }


  // Used to check of the user information is properly stored upon login, and that tha local storage is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])


  // used for hot reload/automatic rendering
  // useEffect is a side effect
  // To implement hot reload efficiently
  useEffect(() => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

        // if user is logged in
        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        } else {
          // if user logs out
          setUser({
            id: null,
            isAdmin: null
          })
        }
    })
  }, []);



  return (
    // user--to store , setUser--taga lagay ng laman, unsetUser--taga alis ng laman
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container>
          <AppNavbar/>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/addCourse" element={<AddCourse />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
