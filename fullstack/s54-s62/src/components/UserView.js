import { useState, useEffect } from 'react';
import CourseCard from './CourseCard.js';
import CourseSearch from './CourseSearch.js';
import PriceSearch from './PriceSearch.js';


// this one also works
// export default function UserView({ coursesData }) {

// 	const [courses, setCourses] = useState([]);

// 	useEffect(() => {

// 		setCourses(coursesData);

// 	}, [coursesData]);

// 	const courseCards = courses.map(course_item => (
// 		<CourseCard key={course_item._id} course={course_item} />
// 		));

// 	return (
// 		<>
// 			{courseCards}
// 		</>
// 	)
// };



// export default function UserView({ coursesData }) {

// 	const [courses, setCourses] = useState([]);

// 	useEffect(() => {
	
// 		setCourses(coursesData);

// 	}, [coursesData]);



// 	return (
// 		<>

// 			<h1>Courses</h1>

// 			{courses.map(course_item => (

// 				<CourseCard key={course_item._id} course={course_item} />
// 			))}
// 		</>
// 	);

// }



// solution
export default function UserView({coursesData}) {

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		const coursesArr = coursesData.map(course => {
			if(course.isActive === true) {
				return (
					<CourseCard courseProp={course} key={course._id} />
				)
			} else {
				return null;
			}
		})

		setCourses(coursesArr);
	}, [coursesData])


	return (

		<>
			<h1 className="text-center">Courses</h1>
			<CourseSearch />
			<PriceSearch />
			{ courses }
		</>
	)

}

