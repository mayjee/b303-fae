/*
my answer -- working

import { useEffect, useState } from 'react';
import EditCourse from './EditCourse';

export default function AdminView({ coursesData, fetchData }) {


  const [courses, setCourses] = useState([]);

  useEffect(() => {
    setCourses(coursesData);
  }, [coursesData]);

  return (
    <>
    
      <h1 className="text-center mt-4 mb-4">Admin Dashboard</h1>

      <table className="table table-striped table-bordered table-hover table-responsive">
        <thead className="text-center">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>
        <tbody>
          {courses.map(course => (
            <tr key={course._id}>
              <td>{course._id}</td>
              <td>{course.name}</td>
              <td>{course.description}</td>
              <td>{course.price}</td>
              <td className={course.isActive ? 'text-success' : 'text-danger'}>
                {course.isActive ? 'Available' : 'Unavailable'}
              </td>
              <td>
                <EditCourse course={course._id} fetchData={fetchData} />
              </td>
              <td>
                <button className="btn btn-danger">Archive</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
*/


import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import ArchiveCourse from './ArchiveCourse';
import EditCourse from './EditCourse';

export default function AdminView({coursesData, fetchData}) {

    const [courses, setCourses] = useState([])

    useEffect(() => {
        const coursesArr = coursesData.map(course => {
            return (
                <tr key={course._id}>
                    <td>{course._id}</td>
                    <td>{course.name}</td>
                    <td>{course.description}</td>
                    <td>{course.price}</td>
                    <td className={course.isActive ? "text-success" : "text-danger"}>
                        {course.isActive ? "Available" : "Unavailable"}
                    </td>
                    <td><EditCourse course={course._id} fetchData={fetchData} /></td>
                    <td><ArchiveCourse course={course._id} isActive={course.isActive} fetchData={fetchData} /></td>
                </tr>
            )
        })

        setCourses(coursesArr)
    }, [coursesData, fetchData])

    return(
        <>
            <h1 className="text-center my-4"> Admin Dashboard</h1>

            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th colSpan="2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {courses}
                </tbody>
            </Table>    
        </>

        )
}

