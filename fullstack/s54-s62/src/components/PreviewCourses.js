import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Product(props) {

	// breakPoint will have something to do with the columns/column distribution of the cards
	const { breakPoint, data } = props;

	const { _id, name, description, price } = data;
	
	return (

		<Col xs={12} md={breakPoint}>
			<Card className="cardHighlight mx-2">
				<Card.Body>
					<Card.Title className="text-center">
						{/* leads to the course view page / prodyct page */}
						<Link to={`/courses/${_id}`}>{name}</Link>
					</Card.Title>

					<Card.Text>
						{description}
					</Card.Text>
				</Card.Body>

				<Card.Footer>
					<h5 className="text-center">Php {price}</h5>
					<Link className="btn btn-primary d-block" to={`/courses/${_id}`}>Details</Link>
				</Card.Footer>
			</Card>
		</Col>
	)
}