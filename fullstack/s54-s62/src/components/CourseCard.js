import {Card} from 'react-bootstrap';
// prop types is built-in
import PropTypes from 'prop-types';
// import { useState } from 'react';
import { Link } from 'react-router-dom';


// improved (destructured)

export default function CourseCard({courseProp}) {

	// const {name, description, price, _id} = courseProp;
	const {name, description, price} = courseProp;

/*
	// a state is used to store information, but dynamic (with getters and setters)
	// kinda similar to 'let count = 0;'
	// count -- getter. used to get the value of count state which can be found in the useState() function, which in this case, 0
	// setCount -- setter. used to modify or update the value of the count state. naming convention -- usually camelcase
	// the useState hook is responsible for setting the initial value of the state
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll() {

		if (seats > 0) {

		// the setCount function can use the previous value of the state and add/modify it
		setCount(prev_value => prev_value + 1);
		setSeats(prev_value => prev_value - 1);
		} else {
			alert("No more seats available.");
		}
	}
*/



// S55 Activity, My Answer by SEAMO

	// const [seats, setSeats] = useState(30);

	// function seat() {
	// 	setSeats(curr_value => curr_value - 1);
	// }

	// function seat() {
	// 	if (seats > 0) {
	// 		setSeats(curr_value => curr_value - 1);
	// 	} else {
	// 		alert("No more seats available");
	// 	}
	// }


	// if (count > 30 && seats < 0) {
	// 	alert("No more seats available");
	// }


	// const handleClick = () => {
	//     enroll();
	//     seat();
	// };


// s55 activity solution



	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php{price}</Card.Text>

				<Link className="btn btn-primary" to="{`/courses/${_id}`}">Details</Link>
			</Card.Body>
		</Card>

	)

}


// PropTypes is used for VALIDATING the data from the props
// by using .propTypes, we will have acces to the prop structure
CourseCard.propTypes = {
	// use .shape() to customize/set the desired structure/data type
	course: PropTypes.shape({
		// if there is no data, for example, the name, it will not return an error but will only show a warning
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

// other npm packages can be used for validating data