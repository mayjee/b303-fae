// my answer
// import {Button, Row, Col} from 'react-bootstrap';

// export default function Banner({ title, subtitle, buttonText, buttonOnClick }) {
// 	return (

// 		<Row>
// 			<Col className="p-5 text-center">
// 				<h1>{ title }</h1>
// 				<p>{ subtitle }</p>
// 				<Button variant="primary" onClick={buttonOnClick}>{ buttonText }</Button>
// 			</Col>
// 		</Row>

// 	)
// }


// solution
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

    console.log(data);
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col className="p-5 text-center">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link className="btn btn-primary" to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}
