// import React, { useState } from 'react';

// const CourseSearch = () => {
//   const [minPrice, setMinPrice] = useState('');
//   const [maxPrice, setMaxPrice] = useState('');
//   const [courses, setCourses] = useState([]);
//   const [isLoading, setIsLoading] = useState(false);

//   const handleSearch = async () => {
//     setIsLoading(true);

//     try {
//       const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify({ minPrice, maxPrice }),
//       });

//       if (response.ok) {
//         const data = await response.json();
//         setCourses(data);
//       } else {
//         console.error('Failed to fetch courses');
//       }
//     } catch (error) {
//       console.error('Error:', error);
//     }

//     setIsLoading(false);
//   };

//   return (
//     <div>
//       <h2>Course Search by Price</h2>
//       <div>
//         <label className="d-block">Min Price:</label>
//         <input
//           type="number"
//           value={minPrice}
//           onChange={(e) => setMinPrice(e.target.value)}
//         />
//       </div>
//       <div>
//         <label className="d-block">Max Price:</label>
//         <input
//           type="number"
//           value={maxPrice}
//           onChange={(e) => setMaxPrice(e.target.value)}
//         />
//       </div>
//       <button className="btn btn-primary" onClick={handleSearch} disabled={isLoading}>
//         {isLoading ? 'Searching...' : 'Search'}
//       </button>

//       <h3>Search Results:</h3>
//       <ul>
//         {courses.map(course => (
//           <li key={course.id}>{course.name} - ${course.price}</li>
//         ))}
//       </ul>
//     </div>
//   );
// };

// export default CourseSearch;



// solution
import React, { useState } from 'react';

const SearchByPrice = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [courses, setCourses] = useState([]);

  const handleMinPriceChange = (e) => {
    setMinPrice(e.target.value);
  };

  const handleMaxPriceChange = (e) => {
    setMaxPrice(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ minPrice, maxPrice }),
    };

    fetch('http://localhost:4000/courses/searchByPrice', requestOptions)
      .then((response) => response.json())
      .then((data) => {
        setCourses(data.courses);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  return (
    <div className="container mb-5">
      <h2>Search Courses by Price Range</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="minPrice" className="form-label">
            Min Price:
          </label>
          <input
            type="number"
            className="form-control"
            id="minPrice"
            value={minPrice}
            onChange={handleMinPriceChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="maxPrice" className="form-label">
            Max Price:
          </label>
          <input
            type="number"
            className="form-control"
            id="maxPrice"
            value={maxPrice}
            onChange={handleMaxPriceChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Search
        </button>
      </form>
      <h3>Search Results:</h3>
      <ul>
        {courses.map((course) => (
          <li key={course.id}>{course.name}</li>
        ))}
      </ul>
    </div>
  );
};

export default SearchByPrice;
