let collection = [];

// Write the queue functions below.


// 1.

function print() {

    return collection;
}




// 2. 3. 5. 6.

function enqueue(name) {

    collection[collection.length] = name;

    return collection;
}




//  4.

function dequeue() {

    for (let i = 0; i < collection.length - 1; i++) {

        collection[i] = collection[i + 1];
    }

    collection.length--;

    return collection;
}




//  7.

function front() {

    return collection[0];
}




//  8.

function size() {

    return collection.length;
}




//  9.

function isEmpty() {

    if (collection.length === 0) {

        return true;

    } else {

        return false;
    }
}












module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};